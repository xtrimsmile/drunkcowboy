﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alco : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<BasePers>();
        if (!player)
        {
            return;
        }

        if (player.Drink(1))
        {
            Destroy(gameObject);
        }
    }
}
