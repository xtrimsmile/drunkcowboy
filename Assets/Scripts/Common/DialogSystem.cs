﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DialogSystem : MonoBehaviour
{
    public event Func<bool> OnCloseSpeech;

    public Text WhoSpeak;
    public Text WhatSpeak;

    public GameObject DialogPanel;
    public GameObject OtherPanel;

    private bool _isDialogOpen;

    private void Update()
    {
        if (!_isDialogOpen)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            CloseSpeech();
        }
    }

    public void Say(string who, string what)
    {
        if (!_isDialogOpen)
        {
            OtherPanel.SetActive(false);
        }
        WhoSpeak.text = who;
        WhatSpeak.text = what;
        if (!_isDialogOpen)
        {
            DialogPanel.SetActive(true);
            _isDialogOpen = true;
        }
    }

    private void CloseSpeech()
    {
        var handler = OnCloseSpeech;
        if (handler != null && handler())
        {
            HideDialog();
        }
    }

    public void HideDialog()
    {
        DialogPanel.SetActive(false);
        OtherPanel.SetActive(true);
    }
}
