﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Func<Bullet, Collider2D, bool> OnBulletWantToDisappear;
    public Action<Bullet, GameObject> OnShoot;

    public float Speed;
    public float MaxDistance;

    [HideInInspector]
    public int BulletIndex;

    protected Rigidbody2D Rigidbody;
    protected Vector3 _startPlace;

    // Update is called once per frame
    private void Update()
    {
        var distance = Mathf.Abs((transform.position - _startPlace).magnitude);
        if (distance >= MaxDistance)
        {
            OnWantToDisappear();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var pers = collision.GetComponent<BasePers>();
        if (pers)
        {
            pers.TakeDamage(1);
        }

        OnWantToDisappear(collision);
    }

    public void Shoot(GameObject sender)
    {
        if (!Rigidbody)
            Rigidbody = GetComponent<Rigidbody2D>();
        if (!Rigidbody)
            return;

        _startPlace = transform.position;

        var shootHandler = OnShoot;
        if (shootHandler != null)
        {
            shootHandler(this, sender);
        }

        gameObject.SetActive(true);
        Rigidbody.velocity = transform.rotation * new Vector2(0, Speed);
    }

    private void OnWantToDisappear(Collider2D collider = null)
    {
        var handler = OnBulletWantToDisappear;
        if (handler == null || handler(this, collider))
        {
            gameObject.SetActive(false);
        }
    }
}
