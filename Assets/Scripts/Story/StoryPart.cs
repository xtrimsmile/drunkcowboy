﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class StoryPart : MonoBehaviour
{
    public BasePers[] Targets;

    public bool IsStart { get; protected set; }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        
    }

    public virtual bool StartState()
    {
        IsStart = false;
        return false;
    }

    public virtual void Tell()
    {
    }

    public virtual bool IsComplete()
    {
        if (Targets == null || Targets.Length == 0)
            return false;

        return Targets.All(_ => !_ || _.Health == 0);
    }
}
