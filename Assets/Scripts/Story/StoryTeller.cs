﻿using UnityEngine;
using System.Collections;

public class StoryTeller : MonoBehaviour
{
    public CameraFollow CameraFollow;
    public DialogSystem Dialog;
    public StoryPart[] Stories;

    private StoryPart _currentStoryPart;

    // Use this for initialization
    private void Start()
    {
        Dialog.OnCloseSpeech += DialogOnCloseSpeechHandler;

        for (int i = 0; i < Stories.Length; i++)
        {
            if (Stories[i].StartState())
            {
                _currentStoryPart = Stories[i];
            }
        }
    }

    private bool DialogOnCloseSpeechHandler()
    {
        return true;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!_currentStoryPart)
        {
            return;
        }

        if (_currentStoryPart.IsComplete())
        {

        }
    }
}
