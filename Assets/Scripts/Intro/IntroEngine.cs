﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UniRx;

public class IntroEngine : MonoBehaviour
{
    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Observable.WhenAll(
                Observable.FromCoroutine(LoadScene))
                .Subscribe(_ => { Debug.Log("end!"); })
                .AddTo(this);
        }
    }

    private IEnumerator LoadScene()
    {
        var progress = SceneManager.LoadSceneAsync("game");

        while (!progress.isDone)
        {
            Debug.Log(progress.progress);
            yield return new WaitForEndOfFrame();
        }
    }
}
