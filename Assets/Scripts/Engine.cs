﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Engine : MonoBehaviour
{
    public PlayerController Player;
    public GameObject[] StartWeapons;
    public Sprite Health;
    public Sprite DrunkHealth;
    public GameObject ParentPanel;
    public List<GameObject> HealthSprites;

    // Use this for initialization
    private void Start()
    {
        if (!Player)
        {
            return;
        }

        if (StartWeapons == null || StartWeapons.Length == 0)
        {
            return;
        }

        for (int i = 0; i < StartWeapons.Length; i++)
        {
            if (!StartWeapons[i])
            {
                continue;
            }

            var weapon = Instantiate(StartWeapons[i]).GetComponent<BaseWeapon>();
            if (weapon)
            {
                Player.TakeWeapon(weapon);
            }
        }

        HealthSprites = new List<GameObject>();
        CheckAndDrawHealth();

        Player.HealthChanged += PlayerHealthChangedHandler;
    }

    // Update is called once per frame
    private void Update()
    {
        if (Player.Health <= 0)
        {
            Observable.WhenAll(
                Observable.FromCoroutine(LoadIntro))
                .Subscribe(_ => { Debug.Log("end!"); })
                .AddTo(this);
        }
    }

    private void PlayerHealthChangedHandler(BasePers arg1, float arg2)
    {
        CheckAndDrawHealth();
    }

    private void CheckAndDrawHealth()
    {
        var parentPosition = ParentPanel.GetComponent<RectTransform>().position;
        var x = 30f;
        var index = 0;
        for (; index < Player.Health + Player.DrunkLevel; index++)
        {
            GameObject imageObj = (HealthSprites.Count > index) ? HealthSprites[index] : new GameObject();
            var image = imageObj.GetComponent<Image>();
            if (!image)
            {
                image = imageObj.AddComponent<Image>();
            }
            image.sprite = index < Player.Health ? Health : DrunkHealth;
            var rect = imageObj.GetComponent<RectTransform>();
            rect.SetParent(ParentPanel.transform);
            rect.anchorMax = new Vector2(0, 0.5f);
            rect.anchorMin = new Vector2(0, 0.5f);
            rect.position = new Vector3(x, parentPosition.y, parentPosition.z);
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 30);
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 30);
            imageObj.SetActive(true);
            x += 40;

            if (HealthSprites.Count <= index)
            {
                HealthSprites.Add(imageObj);
            }
        }

        for (var i = HealthSprites.Count - 1; i >= index; i--)
        {
            var imageObj = HealthSprites[i];
            if (Player.DrunkLevel <= 0)
            {
                HealthSprites.Remove(imageObj);
                Destroy(imageObj);
            }
            else
            {
                imageObj.SetActive(false);
            }
        }
    }

    private IEnumerator LoadIntro()
    {
        var progress = SceneManager.LoadSceneAsync("intro");

        while (!progress.isDone)
        {
            Debug.Log(progress.progress);
            yield return new WaitForEndOfFrame();
        }
    }
}
