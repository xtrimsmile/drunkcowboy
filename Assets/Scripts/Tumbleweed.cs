﻿using UnityEngine;
using System.Collections;

public class Tumbleweed : MonoBehaviour
{
    private Rigidbody2D Rigidbody;
    private Vector3 _startPosition;
    private float _distance;

    private void Start()
    {
        _startPosition = transform.position;
        Rigidbody = GetComponent<Rigidbody2D>();
        Rigidbody.AddForce(new Vector2(Random.Range(-10f, 10f), Random.Range(-10f, 10f)), ForceMode2D.Impulse);
    }

    // Update is called once per frame
    private void Update()
    {
        transform.Rotate(new Vector3(0, 0, 3));

        if (Mathf.Abs((_startPosition - transform.position).magnitude) > 25f)
        {
            Destroy(gameObject);
        }
    }
}
