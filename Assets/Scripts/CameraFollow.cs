﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform FollowObject;
    public float TimeToSwitch = 1f;

    private Camera _main;
    private bool _isOver;
    private Transform _currentFollow;
    private Vector3 _cameraPositionBeforeSwitch;
    private float _switchingTime;

    private void Start()
    {
        _main = Camera.main;
        _currentFollow = FollowObject;
        _isOver = true;
    }

    private void LateUpdate()
    {
        if (_isOver && _currentFollow != FollowObject)
        {
            _isOver = false;
            _currentFollow = FollowObject;
            _cameraPositionBeforeSwitch = _main.transform.position;
            _switchingTime = 0;
        }

        if (_isOver)
        {
            _main.transform.position = new Vector3(_currentFollow.position.x, _currentFollow.position.y, _main.transform.position.z);
        }
        else
        {
            _switchingTime += Time.deltaTime;
            if (_switchingTime > TimeToSwitch)
            {
                _switchingTime = TimeToSwitch;
                _isOver = true;
            }
            var cameraShift = (_currentFollow.position - _cameraPositionBeforeSwitch) * (_switchingTime / TimeToSwitch);
            var cameraPosition = _cameraPositionBeforeSwitch + cameraShift;
            _main.transform.position = new Vector3(cameraPosition.x, cameraPosition.y, _main.transform.position.z);
        }
    }
}
