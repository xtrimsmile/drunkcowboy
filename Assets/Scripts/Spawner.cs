﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject SpawnPrefab;
    public float Delay;
    public bool SpawnOnStart;
    public bool SingleInstance;

    private GameObject _currentInstance;
    private float _lastInstanceSpawnTime;

    // Use this for initialization
    private void Start()
    {
        if (!SpawnOnStart)
        {
            _lastInstanceSpawnTime = Time.time;
            return;
        }

        CreateInstance();
    }

    // Update is called once per frame
    private void Update()
    {
        if (SingleInstance && _currentInstance)
        {
            return;
        }

        if (_lastInstanceSpawnTime + Delay <= Time.time)
        {
            CreateInstance();
        }
    }

    private void CreateInstance()
    {
        if (!SingleInstance || _currentInstance == null)
        {
            _currentInstance = Instantiate(SpawnPrefab);
        }

        _lastInstanceSpawnTime = Time.time;
        ActivateInstance(_currentInstance);
    }

    private void ActivateInstance(GameObject instance)
    {
        instance.transform.position = transform.position;
        instance.transform.rotation = transform.rotation;
        instance.SetActive(true);
    }
}
