﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class BulletManager : MonoBehaviour
{
    private static BulletManager _instance;

    public static BulletManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType<BulletManager>();
            }
            return _instance;
        }
    }

    public GameObject BulletPrefab;
    public int CountBulletsInPool = 100;

    private Bullet[] _bulletsPool;
    private int _currentBulletIndex;

    private Dictionary<Bullet, GameObject> _bulletsParents;

    // Use this for initialization
    public void Start()
    {
        _bulletsPool = new Bullet[CountBulletsInPool];
        _bulletsParents = new Dictionary<Bullet, GameObject>();
    }

    public Bullet GetBullet()
    {
        var index = _currentBulletIndex++;
        if (_currentBulletIndex >= CountBulletsInPool)
        {
            _currentBulletIndex = 0;
        }

        var bullet = _bulletsPool[index];
        if (bullet == null)
        {
            var obj = Instantiate(BulletPrefab);
            bullet = obj.GetComponent<Bullet>();
            bullet.OnShoot = BulletShootHandler;
            bullet.OnBulletWantToDisappear = BulletWantToDisappearHandler;
            bullet.BulletIndex = index;

            _bulletsPool[index] = bullet;
        }

        return bullet;
    }

    private bool BulletWantToDisappearHandler(Bullet bullet, Collider2D collider)
    {
        if (!collider)
        {
            return true;
        }

        if (_bulletsParents[bullet] == collider.gameObject)
        {
            return false;
        }

        return true;
    }

    private void BulletShootHandler(Bullet bullet, GameObject parent)
    {
        _bulletsParents[bullet] = parent;
    }
}
