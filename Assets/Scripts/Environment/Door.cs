﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public float AngVelocityDrop = 180f;
    public float MaxSpringSpeed = 180f;
    public float MaxRotation = 120f;
    public float ZeroAngle = 0f;
    public bool InitZeroAngle = true;

    private Rigidbody2D _rigidbody;
    private bool _back;

    // Use this for initialization
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.centerOfMass = new Vector2();

        if (InitZeroAngle)
        {
            ZeroAngle = transform.rotation.eulerAngles.z;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        //if (Mathf.Approximately(Mathf.Abs(_rigidbody.angularVelocity), 0))
        //    return;

        if (Mathf.Abs(transform.rotation.eulerAngles.z - ZeroAngle) >= MaxRotation
            && (Mathf.Abs(transform.rotation.eulerAngles.z - (ZeroAngle + 360)) >= MaxRotation))
        {
            _rigidbody.angularVelocity = 0;
            return;
        }

        if (Mathf.Approximately(Mathf.Abs(_rigidbody.angularVelocity), 0))
        {
            return;
        }

        var sign = _rigidbody.angularVelocity / Mathf.Abs(_rigidbody.angularVelocity);
        _rigidbody.angularVelocity = Mathf.Lerp(_rigidbody.angularVelocity, _rigidbody.angularVelocity - (AngVelocityDrop * sign), Time.deltaTime);
    }
}
