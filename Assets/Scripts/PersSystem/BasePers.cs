﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePers : MonoBehaviour
{
    public event Action<BasePers, float> HealthChanged;
    public float Health;
    public Transform WeaponPlacement;
    public BaseWeapon CurrentWeapon;

    public bool IsControlEnable = true;

    public void TakeWeapon(BaseWeapon weapon)
    {
        CurrentWeapon = weapon;
        weapon.transform.SetParent(WeaponPlacement);
        weapon.transform.localRotation = new Quaternion();
        weapon.transform.localPosition = new Vector3();
    }

    public virtual void TakeDamage(float value)
    {
        Health -= value;
        OnHealthChanged(-value);

        if (Health <= 0)
        {
            Destroy(gameObject);
        }
    }

    public virtual bool Drink(float value)
    {
        return false;
    }

    protected void OnHealthChanged(float value)
    {
        var handler = HealthChanged;
        if (handler != null)
        {
            handler(this, value);
        }
    }
}
