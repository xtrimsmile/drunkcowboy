﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : BasePers
{
    [HideInInspector]
    public Rigidbody2D Rigidbody;
    // По задумке тут используем стратегию, в реализации отличий не очень много, но они есть.
    public DrunkState[] States;
    public float DrunkLevel;
    public float DrunkReducePerSecond;
    public float Stamina;

    private Camera _mainCamera;
    private int _currentStateIndex;

    public override void TakeDamage(float value)
    {
        if (DrunkLevel > 0)
        {
            DrunkLevel -= value;
            var drunkChanged = value;
            if (DrunkLevel < 0)
            {
                drunkChanged = DrunkLevel + value;
                DrunkLevel = 0;
                var removeFromHealth = -DrunkLevel;
                base.TakeDamage(removeFromHealth);
            }

            OnHealthChanged(-drunkChanged);
            return;
        }

        base.TakeDamage(value);
    }

    public override bool Drink(float value)
    {
        DrunkLevel += value;
        OnHealthChanged(value);
        return true;
    }
    
    private void Start()
    {
        _mainCamera = Camera.main;
        Rigidbody = GetComponent<Rigidbody2D>();

        for (int i = 0; i < States.Length; i++)
        {
            States[i].drunkMovement.Init(this);
            if (i != _currentStateIndex)
            {
                States[i].drunkMovement.gameObject.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (!IsControlEnable)
        {
            return;
        }

        var state = GetCurrentState();
        state.drunkMovement.Move(Time.deltaTime);

        var mousePosition = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
        state.drunkMovement.Turn(mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            state.drunkMovement.Shoot();
        }

        if (DrunkLevel > 0)
        {
            DrunkLevel = Mathf.Max(DrunkLevel - DrunkReducePerSecond * Time.deltaTime, 0);
        }
    }

    private DrunkState GetCurrentState()
    {
        if (States == null || States.Length == 0)
            return null;

        if (States[_currentStateIndex].drunckLevel > DrunkLevel)
        {
            for (int i = _currentStateIndex - 1; i >= 0; i--)
            {
                if (States[i].drunckLevel <= DrunkLevel)
                {
                    States[_currentStateIndex].drunkMovement.gameObject.SetActive(false);
                    States[i].drunkMovement.gameObject.SetActive(true);
                    _currentStateIndex = i;
                    return States[i];
                }
            }
        }
        else
        {
            for (int i = _currentStateIndex; i < States.Length; i++)
            {
                if (i >= States.Length - 1 || States[i + 1].drunckLevel > DrunkLevel)
                {
                    if (_currentStateIndex != i)
                    {
                        States[_currentStateIndex].drunkMovement.gameObject.SetActive(false);
                        States[i].drunkMovement.gameObject.SetActive(true);
                        _currentStateIndex = i;
                    }
                    return States[i];
                }
            }
        }

        return States[_currentStateIndex];
    }
}
