﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidBehavior : BaseDrunkBehavior
{
    public override void Move(float deltaTime)
    {
        _velocity = new Vector2(GetAxisValue(MoveRight, MoveLeft, _velocity.x, deltaTime),
            GetAxisValue(MoveUp, MoveDown, _velocity.y, deltaTime));

        if (_velocity.magnitude > MaxSpeed)
        {
            var coef = Mathf.Sqrt(MaxSpeed / _velocity.magnitude);
            _velocity.x *= coef;
            _velocity.y *= coef;
        }

        _player.Rigidbody.velocity = _velocity;
    }

    public override void Turn(Vector3 mousePosition)
    {
        var angle = GetRotation(mousePosition);
        _player.transform.rotation = Quaternion.Euler(0, 0, -angle);
    }

    public override void Shoot()
    {
        if (_player.CurrentWeapon && _player.CurrentWeapon.CanAttack(_player))
            _player.CurrentWeapon.Attack();
    }

    public override void Evade()
    {
        
    }
}
