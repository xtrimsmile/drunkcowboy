﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotDrunkBehavior : BaseDrunkBehavior
{
    public float MaxShootShift = 10;

    public override void Move(float deltaTime)
    {
        _velocity = new Vector2(GetAxisValue(MoveRight, MoveLeft, _velocity.x, deltaTime),
            GetAxisValue(MoveUp, MoveDown, _velocity.y, deltaTime));

        if (_velocity.magnitude > MaxSpeed)
        {
            var coef = Mathf.Sqrt(MaxSpeed / _velocity.magnitude);
            _velocity.x *= coef;
            _velocity.y *= coef;
        }

        _player.Rigidbody.velocity = _velocity;
    }

    public override void Turn(Vector3 mousePosition)
    {
        var angle = GetRotation(mousePosition);
        _player.transform.rotation = Quaternion.Slerp(_player.transform.rotation, Quaternion.Euler(0, 0, -angle), 0.2f);
    }
    
    public override void Shoot()
    {
        var shift = Random.Range(-MaxShootShift, MaxShootShift);
        if (_player.CurrentWeapon && _player.CurrentWeapon.CanAttack(_player))
            _player.CurrentWeapon.Attack(shift);
    }

    public override void Evade()
    {
        base.Evade();
    }
}
