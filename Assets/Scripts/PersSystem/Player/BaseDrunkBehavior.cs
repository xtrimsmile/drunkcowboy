﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseDrunkBehavior : MonoBehaviour
{
    [HideInInspector]
    public bool isInitialized = false;

    public float MaxSpeed = 10f;
    public float Acceleration = 4f;
    

    public float StaminaRecoverPerSecond;

    public float StaminaForJump = 5;

    public KeyCode MoveUp = KeyCode.W;
    public KeyCode MoveDown = KeyCode.S;
    public KeyCode MoveLeft = KeyCode.A;
    public KeyCode MoveRight = KeyCode.D;
    
    protected KeyCode _defaultMoveUp = KeyCode.W;
    protected KeyCode _defalutMoveDown = KeyCode.S;
    protected KeyCode _defalutMoveLeft = KeyCode.A;
    protected KeyCode _defaultMoveRight = KeyCode.D;

    protected Vector2 _velocity = new Vector2();

    protected PlayerController _player;

    public void Init(PlayerController currentPlayer)
    {
        _player = currentPlayer;
        isInitialized = true;
    }

    public virtual void Move(float deltaTime)
    {
        
    }

    protected float GetAxisValue(KeyCode plusCode, KeyCode minusCode, float startValue, float deltaTime)
    {
        var value = startValue;
        if (Input.GetKey(plusCode))
        {
            value += Acceleration * deltaTime;
        }
        else if (value > 0)
        {
            value = Mathf.Max(value - Acceleration * deltaTime * 5, 0);
        }
        if (Input.GetKey(minusCode))
        {
            value -= Acceleration * deltaTime;
        }
        else if (value < 0)
        {
            value = Mathf.Min(value + Acceleration * deltaTime * 5, 0);
        }

        return value;
    }

    public virtual void Turn(Vector3 mousePosition)
    {
    }

    protected float GetRotation(Vector3 mousePosition)
    {
        var mouseVector = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        return Mathf.Rad2Deg * Mathf.Atan2(mouseVector.x, mouseVector.y);
    }

    public virtual void Shoot()
    {
    }

    public virtual void Evade()
    {
    }
}
