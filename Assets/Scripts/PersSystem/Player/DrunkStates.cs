﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DrunkState
{
    public float drunckLevel;
    public BaseDrunkBehavior drunkMovement;
}
