﻿using UnityEngine;
using System.Collections;

public class ABitWindy : BaseDrunkBehavior
{
    public AnimationCurve DriftCurve;
    public float MaxShootShift = 15;
    public float DriftVelocity = 5;
    public float DriftDuration = 2f;
    public float TurnSpeed = 0.1f;

    private float _driftTimeLeft;
    private int _direction = 1;
    private float _currentDriftTurnMaxSpeed;
    private float _currentDriftVelocity;

    private void OnEnable()
    {
        _currentDriftVelocity = 0;
    }

    private void Update()
    {
        _driftTimeLeft -= Time.deltaTime;

        if (_driftTimeLeft <= 0)
        {
            _driftTimeLeft = DriftDuration;
            _currentDriftTurnMaxSpeed = Random.Range(DriftVelocity / 2, DriftVelocity);
            _direction *= -1;
        }

        if (_direction == 0)
        {
            _driftTimeLeft -= Time.deltaTime;
            return;
        }

        var currentCoefficient = DriftCurve.Evaluate(_driftTimeLeft / DriftDuration);
        _currentDriftVelocity = _currentDriftTurnMaxSpeed * currentCoefficient * _direction;
        if (float.IsNaN(_currentDriftVelocity))
        {
            _currentDriftVelocity = 0;
        }
    }

    public override void Move(float deltaTime)
    {
        _velocity = new Vector2(GetAxisValue(MoveRight, MoveLeft, _velocity.x, deltaTime),
            GetAxisValue(MoveUp, MoveDown, _velocity.y, deltaTime));

        if (_velocity.magnitude > MaxSpeed)
        {
            var coef = Mathf.Sqrt(MaxSpeed / _velocity.magnitude);
            _velocity.x *= coef;
            _velocity.y *= coef;
        }

        var drift = _player.transform.rotation * new Vector2(_currentDriftVelocity, 0);
        _player.Rigidbody.velocity = _velocity + new Vector2(drift.x, drift.y);
    }

    public override void Turn(Vector3 mousePosition)
    {
        var angle = GetRotation(mousePosition);
        _player.transform.rotation = Quaternion.Slerp(_player.transform.rotation, Quaternion.Euler(0, 0, -angle), TurnSpeed);
    }

    public override void Shoot()
    {
        var shift = Random.Range(-MaxShootShift, MaxShootShift);
        if (_player.CurrentWeapon && _player.CurrentWeapon.CanAttack(_player))
            _player.CurrentWeapon.Attack(shift);
    }
}
