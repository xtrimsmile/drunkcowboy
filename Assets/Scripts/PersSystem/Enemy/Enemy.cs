﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : BasePers
{
    public AIDetector Detector;

    public Transform[] ChillPoints;
    public float StayAtPointTime;
    public bool FollowTarget;
    public float DetectDistance = 10f;
    public float AttackDistance = 7f;
    public float ShootDelay = 2f;

    private float _ariveAtPointTime;

    private int _nextPointIndex;
    private Transform _stopPatrolAtPoint;
    private Transform _nextPoint;

    private bool _isTurning;
    private bool _isMoving;

    private bool _seeTarget;
    private Transform _lastTargetPosition;
    private float _lastShootTime;

    private void Update()
    {
        if (!IsControlEnable)
        {
            return;
        }

        if (_seeTarget)
        {
            FollowAndKill();
        }
        else
        {
            Relax(Time.deltaTime);
        }
    }

    private void FollowAndKill()
    {
        if (!Detector.Player)
        {
            _seeTarget = false;
            return;
        }

        _isTurning = TurnToPoint(Detector.Player.transform.position, 1f);
        if (_isTurning)
        {
            return;
        }

        Shoot();
    }

    private void Relax(float deltaTime)
    {
        if (Detector.Player && !_seeTarget)
        {
            var pos = new Vector2(transform.position.x, transform.position.y);
            var direction = transform.rotation * Vector2.up;
            var hits = Physics2D.RaycastAll(pos, new Vector2(direction.x, direction.y), DetectDistance);
            Transform closestTransform = null;
            var minDistance = float.MaxValue;
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].transform.IsChildOf(transform)
                    || (hits[i].transform.IsChildOf(Detector.Player.transform)
                    && hits[i].transform != Detector.Player.transform))
                {
                    continue;
                }

                if (hits[i].distance <  minDistance)
                {
                    minDistance = hits[i].distance;
                    closestTransform = hits[i].transform;
                }
            }

            if (closestTransform == Detector.Player.transform)
            {
                Debug.Log("Got you");
                _seeTarget = true;
                return;
            }
        }

        if (ChillPoints.Length == 0)
        {
            return;
        }

        if (!_nextPoint && ChillPoints.Length > 0)
        {
            _nextPoint = ChillPoints[0];
            _nextPointIndex = 0;
        }

        //if (_isMoving)
        //{
        //    _isMoving = Move(_nextPoint.position);
        //}

        //if (_isTurning)
        //{
        //    _isTurning = TurnToPoint(_nextPoint.position);
        //}
    }

    private bool TurnToPoint(Vector3 point, float rotationSpeed = 0.1f)
    {
        var mouseVector = new Vector2(point.x - transform.position.x, point.y - transform.position.y);
        var angle = -Mathf.Rad2Deg * Mathf.Atan2(mouseVector.x, mouseVector.y);
        if (Mathf.Abs(angle - transform.rotation.eulerAngles.z) < 0.5f
            || Mathf.Abs((angle + 360) - transform.rotation.eulerAngles.z) < 0.5f
            || Mathf.Abs((angle - 360) - transform.rotation.eulerAngles.z) < 0.5f)
            return false;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, angle), rotationSpeed);
        return true;
    }

    private bool Move(Vector3 point)
    {
        return false;
    }

    private void Shoot()
    {
        if (_lastShootTime + ShootDelay < Time.time
            && CurrentWeapon.CanAttack(this))
        {
            _lastShootTime = Time.time;
            CurrentWeapon.Attack();
        }
    }
}
