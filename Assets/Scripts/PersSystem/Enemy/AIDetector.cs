﻿using UnityEngine;
using System.Collections;

public class AIDetector : MonoBehaviour
{
    public PlayerController Player;

    private void Update()
    {
        if (Player && Player.Health <= 0)
        {
            Player = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var detectedPlayer = collision.GetComponent<PlayerController>();
        if (detectedPlayer)
        {
            Player = detectedPlayer;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var detectedPlayer = collision.GetComponent<PlayerController>();
        if (detectedPlayer)
        {
            Player = null;
        }
    }
}
