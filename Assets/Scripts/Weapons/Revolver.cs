﻿using System.Collections.Generic;
using UnityEngine;

public class Revolver : BaseWeapon
{
    public AudioClip ShotSound;

    public int CountOfBulletsInPool = 10;

    protected const string MUZZLE_OBJECT_NAME = "Muzzle";
    protected Transform _muzzle;

    protected AudioSource _audio;

    protected virtual bool CanShoot
    { get { return _muzzle; } }

    // Use this for initialization
    private void Start()
    {
        _muzzle = transform.Find(MUZZLE_OBJECT_NAME);
        _audio = GetComponent<AudioSource>();
    }

    public override bool CanAttack(BasePers pers)
    {
        if (CanShoot && _lastShootTime + FireRate <= Time.time)
            return true;

        return base.CanAttack(pers);
    }

    public override void Attack(float shift = 0)
    {
        var bullet = BulletManager.Instance.GetBullet();

        if (bullet)
        {
            var euler = _muzzle.rotation.eulerAngles;
            bullet.transform.position = _muzzle.position;
            bullet.transform.eulerAngles = new Vector3(euler.x, euler.y, euler.z + shift);
            bullet.Shoot(gameObject);
        }
        if (_audio)
        {
            _audio.PlayOneShot(ShotSound);
        }

        _lastShootTime = Time.time;
    }
}
