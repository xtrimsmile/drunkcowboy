﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseWeapon : MonoBehaviour
{
    public float FireRate;

    protected float _lastShootTime;

    public virtual bool CanAttack(BasePers pers)
    {
        return false;
    }

    public virtual void Attack(float shift = 0)
    {
        Debug.Log("Attack!");
    }
}
